<?php

use Illuminate\Http\Request;
use App\Http\Controllers\ {
    IndexController,
    CompanyController,
    PersonalController
};
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

Route::get('/', IndexController::class)->name('index');


/*
|-------------------------------------------
| Форму ввода для добавления организации
|------------------------------------------
|
*/
Route::post('/new-company', CompanyController::class);


/*
|-------------------------------------------
| Форма ввода для добавления нового сотрудника
|------------------------------------------
| - при добавление проверяется e-mail на уникальность
| - запускается фоновая задача, для пересчета общего кол-ва
|   сотрудников в организ-и
| - сохраняется результат как в таблицу Personal
| - так и в таблицу company в поле total_personal
|
*/
Route::post('/new-personal', PersonalController::class);
