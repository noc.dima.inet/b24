<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\Request;

use App\Models\ {
    Personal,
    Company
};

class NewСompanyTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;


    public function test_company_insert()
    {

        Company::insert([
            'name_company'   => 'Новая компания',
            'total_personal' => '4',
        ]);
        
        $this->assertDatabaseHas('company', [
            'name_company' => 'Новая компания',
        ]);

        $response = $this->get('/');

        $response->assertStatus(200);
    }






    
}
