<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\ {
    Personal,
    Company
};
class IndexPageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_home_page_status()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_get_company_all()
    {
        $companyAll = Company::all();

        $response = $this
            ->get('/')
            ->assertViewHasAll(['companyAll' => $companyAll]
        );

        
    }






}
