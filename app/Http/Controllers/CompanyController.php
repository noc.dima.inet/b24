<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ {
    Personal,
    Company
};

class CompanyController extends Controller
{
    public function __invoke(Request $request)
    {
        $companyName = $request->input('companyName');

        Company::insert([
            'name_company' => $companyName,

        ]);

        return redirect('/');
    }
}
