<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\ {
    Personal,
    Company
};
use Faker\Provider\ar_EG\Person;

class IndexController extends Controller
{


    public function __invoke()
    {
        $companyAll = Company::all();

        return view('index', [
            'companyAll' => $companyAll,
        ]);
     
    }

}
