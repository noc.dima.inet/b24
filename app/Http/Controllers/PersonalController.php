<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ {
    Personal,
    Company
};

class PersonalController extends Controller
{
    public function __invoke(Request $request)
    {
        $name         = $request->input('name');
        $companyId    = $request->companyId;
        $email        = $request->input('email');

        // $getEmail - для проверки существования мыла
        // $getEmail = Personal::where('email', $email)->first()->email;
 

        $request->validate([
            'name' => 'required',
            'email' => 'unique:personal',
        ]);

        Personal::insert([
            'company_id' => $companyId,
            'name'       => $name,
            'email'      => $email,
        ]);

        $totalPersonal = Company::find($companyId)->total_personal;
        
        DB::table('company')
            ->where('id', '=', $companyId)
            ->update(['total_personal' => $totalPersonal + 1 ]);

        return redirect('/');
    }


    
}
