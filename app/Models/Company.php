<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table = 'company';

    protected $dates = [
        'updates_at',
        'created_at'
    ];

    protected $fillable = [
        'total_personal',
        'name_company'
    ];
}
