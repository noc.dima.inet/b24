@extends('layout')

@section('content')
    <h1>Привет!</h1>
    <div >
        <p>Количество работников в компании</p>
        <table style="border:1px solid #ccc; padding:10px;">
            <tr>
                <th> id </th>
                <th>Название компании</th>
                <th>Кол-во</th>
            </tr>
           @foreach ($companyAll as $company)
             <tr>
                <td>{{ $company->id }}</td>
                <td>{{ $company->name_company }}</td>
                <td>{{ $company->total_personal }}</td>
             </tr>
           @endforeach
        </table>
    </div>
        <hr>
    <p>Новая компания</p>
    <form action="/new-company" method="POST">
        <input name="companyName">
        {{ csrf_field() }}
        <button type="submit">Ввод</button>
    </form>
    <hr>
    <!-- Добавление нового сотрудника -->
    <p>Новый сотрудник</p>
    <form action="/new-personal" method="POST">

        {{-- lang/en/validation.php --}}
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <span style="color:red">{{ $error }} </span>
            @endforeach
        @endif
        <br/>
        <input name="name" placeholder="Введите имя">&nbsp;


        <input name="email" placeholder="Введите e-mail*">&nbsp;

        <select name="companyId" >
          @foreach ($companyAll as $company )
            <option value="{{ $company->id }}">
                {{ $company->name_company }}
            </option>
           @endforeach
        </select>
        <br><br>
        {{ csrf_field() }}
        <button type="submit">Ввод</button>
        <p>*e-mail уникальный для каждого юзера</p>
    </form>

@endsection
